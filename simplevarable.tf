provider "aws" {
    region = "ap-south-1"
}

variable "ami_id" {
    type = string
    default = "ami-0a23ccb2cdd9286bb"
}

resource  "aws_instance" "dev" {
    ami = "${var.ami_id}"
    instance_type = "t2.micro"
    key_name = "Mumbai_Aug"
    tags = {
        Env = "devvlopment"
        Name = "dev"
    }
}