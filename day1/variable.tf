variable "ami_id" {
  type        = string
  default     = "ami-0a23ccb2cdd9286bb"
}

variable "instance_type_list" {
    type = list
    default = ["t2.micro", "t2.small", "t2.medium "]
}

variable "my_tag" {
    type = map
    default = {
        env = "dev"
        name = "xyz"
    }
}

variable "key" {
    type = string
    default = "Mumbai_Aug"
}
