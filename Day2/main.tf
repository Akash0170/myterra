resource "aws_vpc" "my_vpc" {
    cidr_block = "${var.VPC_CIDR}"
    tags = {
        Name = "my_vpc"
        Env = "${var.Tags}"
    }
}

resource "aws_subnet" "Myprivatesubnet" {
    vpc_id = "${aws_vpc.my_vpc.id}"
    cidr_block = "${var.VPC_Private_CIDR}"
    tags = {
        Name = "Myprivatesubnet"
        Env = "${var.Tags}"
    }
}

resource "aws_subnet" "MyPublicsubnet" {
    vpc_id = "${aws_vpc.my_vpc.id}"
    cidr_block = "${var.VPC_Public_CIDR}"
    map_public_ip_on_launch = true
    tags = {
        Name = "MyPublicsubnet"
        Env = "${var.Tags}"
    }
}

resource "aws_internet_gateway" "my_igw" {
    vpc_id = "${aws_vpc.my_vpc.id}"
    tags = {
        Name = "my_igw"
        Env = "${var.Tags}"
    }
}

resource "aws_default_route_table" "my_rt" {
    default_route_table_id = "${aws_vpc.my_vpc.default_route_table_id}"
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.my_igw.id}"
    }
    tags = {
        Name = "my_rt"
        Env = "${var.Tags}"
    }
}

resource "aws_security_group" "my_sg" {
    name = "my_sg_group"
    description = "allowing http, ssh, https"
    vpc_id = "${aws_vpc.my_vpc.id}"
    ingress {
        description = "SSH Port"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        description = "HTTP Port"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        description = "out bound all rules"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
}

resource "aws_instance" "finally_privateinst_here" {
    ami = "${var.ami_id}"
    instance_type = "${var.inst_type}"
    tags = {
        Name = "Private"
        Env = "${var.Tags}"
    }
    vpc_security_group_ids = ["${aws_security_group.my_sg.id}"]
    subnet_id = "${aws_subnet.Myprivatesubnet.id}"
    key_name = "Mumbai_Aug"
}

resource "aws_instance" "finally_Pubinst_here" {
    ami = "${var.ami_id}"
    instance_type = "${var.inst_type}"
    tags = {
        Name = "Public"
        Env = "${var.Tags}"
    }
    vpc_security_group_ids = ["${aws_security_group.my_sg.id}"]
    subnet_id = "${aws_subnet.MyPublicsubnet.id}"
    user_data = <<-EOF
        #!/bin/bash
        yum install httpd -y
        systemctl start httpd
        systemctl enable httpd
        echo "hello world" > /var/www/html/index.html
        EOF
    key_name = "Mumbai_Aug"
}
