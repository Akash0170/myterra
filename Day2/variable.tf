variable "VPC_CIDR" {
    description = "VPC CIDR value"
    type = string
    default = "172.25.0.0/16"
}

variable "VPC_Private_CIDR" {
    description = "Private CIDR"
    type = string
    default = "172.25.0.0/20"
}

variable "VPC_Public_CIDR" {
    description = "Public CIDR"
    type = string
    default = "172.25.16.0/20"
}

variable "Tags" {
    default = "Dev"
    type = string
}

variable "ami_id" {
    default = "ami-06a0b4e3b7eb7a300"
    type = string
}

variable "inst_type" {
    default = "t2.micro"
    type = string
}
