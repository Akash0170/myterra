resource "aws_instance" "xyz"{
    ami = "${var.ami_id}"
    instance_type = "${var.instance_type_list[1]}"
    tags = "${var.my_tag}"
    key_name = "${var.key}"
}