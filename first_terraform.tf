provider "aws" {
    region = "ap-south-1"
}

resource  "aws_instance" "dev" {
    ami = "ami-0a23ccb2cdd9286bb"
    instance_type = "t2.micro"
    key_name = "Mumbai_Aug"
    tags = {
        Env = "devvlopment"
        Name = "dev"
    }
}
